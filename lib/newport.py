"""
Adapted from Ben Hammel's interface:
    https://github.com/bdhammel/python_newport_controller
"""
from __future__ import annotations
import usb.core
import usb.util
import re
import time

CMDREGEX = re.compile(r'([1-4]?)([a-zA-Z]{2})([?]?)([0-9+-]*)')
SPCMDREGEX = re.compile(r'(\*[a-zA-Z]{3})([?]?)([01]?)')
MOTOR_TYPE = {
    "0": "No motor connected",
    "1": "Motor unknown",
    "2": "'Tiny' motor",
    "3": "'Standard' motor",
}
MAX_STEPS = 320000

class PicoController:
    """
    Driver class. Methods that don't have an annotated return value return self
    instead.

    Attributes
    ----------
    product_id : int
    vendor_id : int
    device : usb.core.Device
    endpoint_in : usb.util.Descriptor
    endpoint_out : usb.util.Descriptor
    """

    def __init__(self, product_id: int=0x4000, vendor_id: int=0x104d):
        """
        Constructor.

        Parameters
        ----------
        product_id, vendor_id : int (hex)
        """
        self.product_id = product_id
        self.vendor_id = vendor_id

    def connect(self, printflag: bool=False):
        """
        Connect to the USB device.
        """
        # find the device and initialize
        self.device = usb.core.find(
            idProduct=self.product_id, idVendor=self.vendor_id)
        if self.device is None:
            raise ValueError(
                f"Cannot find device ({self.product_id}, {self.vendor_id})")
        self.device.set_configuration()


        # get input/output endpoints
        cfg = self.device.get_active_configuration()
        intf = cfg[(0,0)] # indexed by (interface idx, alternate setting idx)
        self.endpoint_in = usb.util.find_descriptor(
            intf,
            custom_match=lambda e:
                usb.util.endpoint_direction(e.bEndpointAddress) \
                        == usb.util.ENDPOINT_IN
        )
        self.endpoint_out = usb.util.find_descriptor(
            intf,
            custom_match=lambda e:
                usb.util.endpoint_direction(e.bEndpointAddress) \
                        == usb.util.ENDPOINT_OUT
        )
        if (self.endpoint_in and self.endpoint_out) is None:
            raise ValueError(
                f"Cannot find endpoints ({self.product_id}, {self.vendor_id})")

        # confirm connection
        if printflag:
            resp = self.command("VE?")
            print(
                "Connected to controller: model {}; firmware {} {} {}" \
                        .format(*resp.split(' '))
            )
            for m in range(1, 5):
                resp = self.command(f"{m}QM?")
                print("  Motor #{}: {}".format(m, MOTOR_TYPE[resp[-1]]))
        return self

    def send_command(self, usb_command: str, get_reply: bool=False) \
            -> (str, None):
        """
        Send a command to the USB device endpoint.

        Parameters
        ----------
        usb_command : str
            Formatted command for the USB driver.
        get_reply : bool
            Query endpoint_in after sending the command.

        Returns
        -------
        reply : str
            ASCII representation of the response from the endpoint if get_reply
            is True, otherwise None.
        """
        self.endpoint_out.write(usb_command)
        return self.endpoint_in.read(100) if get_reply else None

    def parse_command(self, newfocus_command: str) -> str:
        """
        Convert a NewFocus command into a USB command.

        Parameters
        ----------
        newfocus_command : str
            Command in the NewFocus format, which is xxAAnn, where AA is the
            case-insensitive mnemonic and xx and nn are required or optional,
            depending on the value of AA (see section 6.2 of the manual).
        """
        if match := CMDREGEX.match(newfocus_command):
            driver, command, expect, arg = match.groups()
            #print(driver, command, expect, arg)
            cmd = command + expect
            if driver:
                cmd = f"1>{driver} {cmd}"
            if arg:
                cmd = f"{cmd} {arg}"
            cmd += '\r'
            #print(cmd)
            return cmd
        elif SPCMDREGEX.match(newfocus_command):
            return newfocus_command
        else:
            raise ValueError(
                f"Encountered mal-formatted command: {newfocus_command}")

    def parse_reply(self, newfocus_reply: list[bytes]) -> str:
        """
        Interpret the response to a query.

        Parameters
        ----------
        newfocus_reply : list[bytes]
            Response to query.

        Returns
        -------
        reply : str
            Cleaned string of controller response.
        """
        return "".join(chr(x) for x in newfocus_reply) \
                .rstrip().replace("1>", "")

    def command(self, newfocus_command: str) -> str:
        """
        Send a NewFocus-formatted command with response handling.

        Parameters
        ----------
        newfocus_command : str
            Command in NewFocus format described in section 6.2 of the manual.

        Returns
        -------
        reply : str
            Cleaned controller response if expected, otherwise None.
        """
        command = self.parse_command(newfocus_command)
        reply = self.send_command(command, "?" in newfocus_command)
        return self.parse_reply(reply) if reply else None

    def id(self) -> str:
        """
        Get the unique identification string for the controller.

        Returns
        -------
        id : str
        """
        return self.command("*IDN?")

    def recall(self, arg: int):
        """
        Restore working parameters from non-volatile memory.

        Parameters
        ----------
        arg : int in {0, 1}
            0 -> Restore factory default settings
            1 -> Restore last saved settings
        """
        assert arg in {0, 1}
        self.command(f"*RCL{arg}")
        return self

    def reset(self):
        """
        Perform a soft reset of the controller CPU.
        """
        self.command("*RST")
        return self

    def abort_motion(self):
        """
        Instantaneously (and abruptly) stop any motion currently in progress.
        """
        self.command("AB")
        return self

    def set_acceleration(self, xx: int, nn: int):
        """
        Set the acceleration value for an axis. Will not affect any move already
        in progress. Values will be accepted but only applied to subsequent
        moves.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.
        nn : int in {1, ..., 200_000}
            Acceleration in steps/s^2.
        """
        assert nn in range(1, 200001)
        self.command(f"{xx}AC{nn:+}")
        return self

    def get_acceleration(self, xx: int) -> int:
        """
        Get the current acceleration value for an axis.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.

        Returns
        -------
        ac : int
            Acceleration in steps/s^2
        """
        return int(self.command(f"{xx}AC?"))

    def set_home_position(self, xx: int, nn: int=0):
        """
        Set the "home" position for an axis. Upon receipt, the present position
        will be set to the specified home position.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.
        nn : int in {-2^31 + 1, ..., 2^31 - 1}
            Home position in steps.
        """
        assert nn in range(-2**31 + 1, 2**31)
        self.command(f"{xx}DH{nn:+}")
        return self

    def get_home_position(self, xx: int) -> int:
        """
        Get the current home position for an axis.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.

        Returns
        -------
        dh : int
            Home position in steps.
        """
        return int(self.command(f"{xx}DH?"))

    def motor_check(self):
        """
        Perform a motor check for all axes by moving one step in the negative
        direction, then one in the positive direction for all axes.
        """
        self.command("MC")
        return self

    def is_motion_done(self, xx: int) -> int:
        """
        Query the motion status for an axis.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.

        Returns
        -------
        md : int
            Motion status:
                0 -> Motion is in progress
                1 -> Motion is not in progress
        """
        return int(self.command(f"{xx}MD?"))

    def indefinite_move(self, xx: int, nn: str):
        """
        Begin indefinite motion on an axis.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.
        nn : str in {'+', '-'}
            Direction of motion.
        """
        assert nn in {'+', '-'}
        self.command(f"{xx}MV")
        return self

    def set_position(self, xx: int, nn: int):
        """
        Move an axis to a target position relative to the home position.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.
        nn : int in {-2^31 + 1, ..., 2^31 - 1}
            Position in steps.
        """
        assert nn in range(-2**31 + 1, 2**31)
        self.command(f"{xx}PA{nn:+}")
        return self

    def get_position(self, xx: int) -> int:
        """
        Get the current position of an axis.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.

        Returns
        -------
        pa : int
            Position in steps.
        """
        return int(self.command(f"{xx}PA?"))

    def set_position_rel(self, xx: int, nn: int):
        """
        Move an axis to a target position relative to the current position.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.
        nn : int in {-2^31 + 1, ..., 2^31 - 1}
            Relative position in steps.
        """
        self.command(f"{xx}PR{nn:+}")
        return self

    def get_position_target(self, xx: int) -> int:
        """
        Get the current target position of an axis.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.

        Returns
        -------
        pr : int
            Target position in steps.
        """
        int(self.command(f"{xx}PR?"))
        return self

    def set_motor_type(self, xx: int, nn: int):
        """
        Set the motor type of an axis.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.
        nn : int in {0, ..., 3}
            Motor type:
                0 -> No motor
                1 -> Unknown motor
                2 -> 'Tiny' motor
                3 -> 'Standard' motor
        """
        assert nn in range(0, 4)
        self.command(f"{xx}QM{nn}")
        return self

    def get_motor_type(self, xx: int) -> int:
        """
        Get the motor type of an axis.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.

        Returns
        -------
        qm : int
            Motor type.
        """
        return int(self.command(f"{xx}QM?"))

    def set_address(self, nn: int=1):
        """
        Set the address of the controller.

        Parameters
        ----------
        nn : int in {1, ..., 31}
        """
        assert nn in range(1, 32)
        self.command(f"SA{nn}")
        return self

    def get_address(self) -> int:
        """
        Get the current address of the controller.

        Returns
        -------
        sa : int
            Current address (1-31).
        """
        return int(self.command("SA?"))

    def network_scan(self, nn: int):
        """
        Scan for slave controllers over RS-485.

        Parameters
        ----------
        nn : int in {0, 1, 2}
            Conflict resolution strategy:
                0 -> Scan the network but do not resolve conflicts.
                1 -> Scan the network and reassign conflicting addresses to the
                    lowest available.
                2 -> Scan the network and reassign all addresses in sequential
                    order, beginning with the master controller at 1.
        """
        assert nn in range(0, 3)
        self.command(f"SC{nn}")
        return self

    def network_map(self) -> int:
        """
        Return a 32-bit int where the value of the k-th bit for k > 0 indicates
        the presence of a controller at address k. For the k = 0-th bit, a 0
        indicates no address conflicts and a 1 indicates at least one.
        """
        return int(self.command(f"SC?"))

    def is_network_scan_done(self) -> int:
        """
        Query the status of a network scan.

        Returns
        -------
        sd : int
            Status of the scan:
                0 -> Scan is in progress
                1 -> Scan is not in progress
        """
        return int(self.command(f"SD?"))

    def save_settings(self):
        """
        Save current controller settings to non-volatile memory.
        """
        self.command("SM")
        return self

    def stop(self, xx: int):
        """
        Stop the motion of an axis.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.
        """
        self.command(f"{xx}ST")
        return self

    def get_error(self) -> (int, str):
        """
        Pop an error off the internal FIFO buffer. Note that said buffer is 10
        elements deep.

        Returns
        -------
        nnn : int
            Error code with first digit corresponding to the axis generating the
            error. An error code of 0 indicates no error.
        msg : str
            Associated error message.
        """
        nnn, msg = self.command("TB?").split(", ")
        return int(nnn), msg

    def get_error_code(self) -> int:
        """
        Pop an error off the internal FIFO buffer and return only the error
        code. Note that said buffer is 10 elements deep.

        Returns
        -------
        nnn : int
            Error code with first digit corresponding to the axis generating the
            error. An error code of 0 indicates no error.
        """
        return int(self.command("TE?"))

    def get_actual_position(self, xx: int) -> int:
        """
        Get the "actual" position of an axis, representing the internal number
        of steps made by the controller relative to its position when the
        controller was powered on OR a system reset occurred OR a set-home (DH)
        command was received.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.

        Returns
        -------
        nn : int
            Current position in steps.
        """
        return int(self.command(f"{xx}TP?"))

    def set_velocity(self, xx: int, nn: int):
        """
        Set the velocity value for an axis. Will not affect any move already in
        progress. The maximum velocity for a "Tiny" motor is 1750; this value is
        not checked.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.
        nn : int in {1, ..., 2000}
            Velocity in steps/s
        """
        assert nn in range(1, 2001)
        self.command(f"{xx}VA{nn:+}")
        return self

    def get_velocity(self, xx: int) -> int:
        """
        Get the current velocity for an axis.

        Parameters
        ----------
        xx : int in {1, ..., 4}
            Axis number.

        Returns
        -------
        nn : int
            Current velocity value.
        """
        return int(self.command(f"{xx}VA?"))

    def get_firmware_ver(self) -> str:
        """
        Get the current controller firmware version.

        Returns
        -------
        ve : str
            Current firmware version.
        """
        return self.command("VE?")

    def purge_memory(self):
        """
        Purge all user settings in controller memory.
        """
        self.command("XX")
        return self

    def set_config_register(self, nn: int):
        """
        Set the default behavior of some features.

        Parameters
        ----------
        nn : int in {0, ..., 3}
            2-bit int giving a configuration:
            
            Bit#    Value   Description
            ----    -----   -----------
            0       0       Perform auto motor detection. Check and set motor
                            type automatically when commanded to move.

            0       1       Do not perform auto motor detection on move.

            1       0       Do not scan for motors connected to controllers on
                            boot.

            1       1       Scan for motors connected to controller on boot.
        """
        assert nn in range(0, 4)
        self.command(f"ZZ{nn}")
        return self

    def get_config_register(self) -> int:
        """
        Get the current default behavior of some features.

        Returns
        -------
        nn : int in {0, ..., 3}
            2-bit int giving a configuration:
            
            Bit#    Value   Description
            ----    -----   -----------
            0       0       Perform auto motor detection. Check and set motor
                            type automatically when commanded to move.

            0       1       Do not perform auto motor detection on move.

            1       0       Do not scan for motors connected to controllers on
                            boot.

            1       1       Scan for motors connected to controller on boot.
        """
        return int(self.command("ZZ?"))

    def wait_motion(self, xx: int, dt: float=0.5):
        """
        Query the motion status every `dt` seconds until motion stops.
        """
        while not self.is_motion_done(xx):
            time.sleep(dt)
        return self

